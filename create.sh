#!/bin/bash

BOARD_HOME=/home/manuel/projets/bcel/docker-atlasboard

sudo docker run --rm -ti --hostname atlasboard --name atlasboard -p 3000:3000 \
                --env NEXUS_NPM_TOKEN=bnBtLWNpOkdQZzN1NW42QGRYQnkkN24= \
                --env AWS_HOST=52.209.13.191 \
                --env NEXUS_URL_NPM_REPO=8081/repository/npm-repo/ \
                --env BITBUCKET_USER=$BITBUCKET_USER \
                --env BITBUCKET_PWD=$BITBUCKET_PWD \
                -v $BOARD_HOME/node_modules:/home/node/teamboard/node_modules/:rw \
                -v $BOARD_HOME/node_modules:/home/node/teamboard/packages/atlassian/node_modules/:rw \
                -v $BOARD_HOME/node_modules:/home/node/teamboard/packages/red6/node_modules/:rw \
                -v $BOARD_HOME/node_modules:/home/node/teamboard/packages/bcel/node_modules/:rw \
                -v $BOARD_HOME/atlasboard-bcel-package:/home/node/teamboard/packages/bcel/:rw \
                -v $BOARD_HOME/dashboard:/home/node/teamboard/packages/default/dashboards/:rw \
                bcelouest/atlasboard $1
