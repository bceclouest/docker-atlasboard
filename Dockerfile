FROM node:7-alpine
MAINTAINER Timwi Consulting <contact@timwi.com>

RUN apk update && apk upgrade && \
    apk add --no-cache bash git openssh sudo

RUN echo "node ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/node

ENV NEXUS_NPM_TOKEN ${NEXUS_NPM_TOKEN}
ENV AWS_HOST ${AWS_HOST}
ENV NEXUS_URL_NPM_REPO ${NEXUS_URL_NPM_REPO}

USER node
WORKDIR /home/node

ADD npmrc /home/node/.npmrc
ADD bashrc /home/node/
RUN cat /home/node/bashrc >> /home/node/.bashrc && rm /home/node/bashrc

RUN sudo npm install -g atlasboard

RUN atlasboard new teamboard
#&& cd teamboard
#&& npm install

WORKDIR teamboard

RUN git init
RUN git submodule add https://github.com/red6/atlasboard-red6-package.git packages/red6
RUN git submodule add https://bitbucket.org/atlassian/atlasboard-atlassian-package packages/atlassian

RUN rm -rf packages/*/dashboards
RUN rm -rf packages/default packages/demos packages/demo

ADD globalAuth.json .

ADD start.sh .

EXPOSE 3000

ENV NEXUS_NPM_TOKEN ""
ENV AWS_HOST ""
ENV NEXUS_URL_NPM_REPO ""

#CMD ["atlasboard", "start"]
CMD ["./start.sh"]
#CMD ["bash"]
