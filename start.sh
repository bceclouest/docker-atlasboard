#!/bin/bash

#mkdir node_modules
#sudo chown -R node: node_modules/
#sudo chown -R node: packages/*/node_modules/

npm install

if [[ -d packages/bcel ]]; then
    cd packages/bcel
    git pull
    cd ../..
else
    git submodule add https://bitbucket.org/bceclouest/atlasboard-bcel-package packages/bcel
fi

atlasboard start
